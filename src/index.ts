import { ComponentExample } from "@/components";
import type { SomeType } from "@/types/SomeType";
import vuetify from "@/plugins/vuetify";

export { vuetify, ComponentExample };
export type { SomeType };
