import { describe, test } from "vitest";
import { mount } from "@vue/test-utils";
import { createVuetify } from "vuetify";

import ComponentExample from "../ComponentExample.vue";

describe("ComponentExample.vue", () => {
  test("should mount component", () => {
    const vuetify = createVuetify();
    mount(ComponentExample, {
      global: {
        plugins: [vuetify],
      },
    });
  });
});
